//Ahmed Sobh
//Student Id: 2134222
package linearalgebra;
import java.lang.Math;

public class Vector3d{

    private double x;
    private double y;
    private double z;

    public Vector3d (double x, double y, double z){

        this.x = x;
        this.y = y;
        this.z = z;

    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        return Math.sqrt((Math.pow(x,2))+(Math.pow(y,2))+(Math.pow(z,2)));
    }

    public double dotProduct(Vector3d secondVector){
        return (this.x * secondVector.getX()) + (this.y * secondVector.getY()) + (this.z * secondVector.getZ());
    }

    public Vector3d add(Vector3d secondVector){
        Vector3d returnedVector = new Vector3d(this.x + secondVector.getX(), this.y + secondVector.getY(), this.z + secondVector.getZ());
        return returnedVector;
    }

    public static void main(String args[]){
        Vector3d firstVector = new Vector3d(1,2,3);
        Vector3d secondVector = new Vector3d(4,5,6);

        double mag = firstVector.magnitude();
        System.out.println(mag);

        double dotProduct = firstVector.dotProduct(secondVector);
        System.out.println(dotProduct);

        Vector3d testVector = new Vector3d(1,2,3);
        testVector = firstVector.add(secondVector);

        double testX = testVector.getX();
        double testY = testVector.getY();
        double testZ = testVector.getZ();

        System.out.println(testX + " " + testY + " " + testZ);
    }

}