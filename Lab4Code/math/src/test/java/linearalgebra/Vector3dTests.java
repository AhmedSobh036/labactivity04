//Ahmed Sobh
//Student ID: 2134222
package linearalgebra;

import static org.junit.Assert.*;

import org.junit.Test;

public class Vector3dTests{
    /**
     * 
     */
    @Test
    public void testGetXYZ(){
        Vector3d testVector = new Vector3d(1, 2, 3);
        assertEquals(1,testVector.getX(),0);
        assertEquals(2,testVector.getY(),0);
        assertEquals(3,testVector.getZ(),0);
    }

    @Test
    public void testMagnitude(){
        Vector3d firstVector = new Vector3d(1, 2, 3);
        //Vector3d secondVector = new Vector3d(4, 5, 6);
        assertEquals(3.7416573867739413, firstVector.magnitude(), 0);
    }

    @Test
    public void testDotProduct(){
        Vector3d firstVector = new Vector3d(1, 2, 3);
        Vector3d secondVector = new Vector3d(4, 5, 6);

        assertEquals(32, firstVector.dotProduct(secondVector), 0);
    }

    @Test
    public void testAdd(){
        Vector3d firstVector = new Vector3d(1, 2, 3);
        Vector3d secondVector = new Vector3d(4, 5, 6);
       
        Vector3d testVector = new Vector3d(1,2,3);
        testVector = firstVector.add(secondVector);

        assertEquals(5, testVector.getX(), 0);
        assertEquals(7, testVector.getY(), 0);
        assertEquals(9, testVector.getZ(), 0);
    }
    
}